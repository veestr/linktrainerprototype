Senior Design Project to modernize a Link Trainer to be moved by output sent from X-Plane 10 fight sim given linear 
potentiometer input from joystick, rudder, and throttle.

Prototype is a mock of actual trainer, it's being used to develop code on a Pi 3 B to implement a system design.

Work in progress on code to be posted later.